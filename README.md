# Présentation

J'ai choisi le jeu Memory Card sur le thème de Mario
<br>

# Maquette

![wireframe Game](public/img/maquette1.png)
![wireframe Game](public/img/maquette2.png)

# Fonctionnalités

- Afficher les cartes
- Décrémenter le timer
- Incrémenter le score à chaque match
- Retourner 2 cartes maximum
- Si 2 mêmes cartes retournées -> Incrémenter le score
-
<br>

# Compétences utilisées
- classe
- tableaux d'objet
- boucles
- DOM
- condition 
- 
<br>

# Lien site

[Lien vers mon jeu](https://ghanemijehane.gitlab.io/game-js)
