import {Score} from './Score'
let cardsArray = [
  {
    name: 'shell',
    img: './img/blueshell.png',
  },
  {
    name: 'star',
    img: './img/star.png',
  },
  {
    name: 'bobomb',
    img: './img/bobomb.png',
  },
  {
    name: 'mario',
    img: './img/mario.png',
  },
  {
    name: 'luigi',
    img: './img/luigi.png',
  },
  {
    name: 'peach',
    img: './img/peach.png',
  },
  {
    name: '1up',
    img: './img/1up.png',
  },
  {
    name: 'mushroom',
    img: './img/mushroom.png',
  },
  {
    name: 'thwomp',
    img: './img/thwomp.png',
  },
  {
    name: 'bulletbill',
    img: './img/bulletbill.png',
  },
  {
    name: 'coin',
    img: './img/coin.png',
  },
  {
    name: 'goomba',
    img: './img/goomba.png',
  },

]


// Duplicate array to create a match for each card
let gameGrid = cardsArray.concat(cardsArray);

// Randomize game grid on each load
gameGrid.sort(() => 0.5 - Math.random());


let firstGuess = '';
let secondGuess = '';
let count = 0;
let previousTarget = null;
let delay = 1200;
let gamePlay = new Audio("./gameplay.mp3");

let game = document.getElementById('game');
let grid = document.createElement('section');
grid.setAttribute('class', 'grid');
game.appendChild(grid);

let score = new Score();

console.log(score)




// For each item in the cardsArray array...
gameGrid.forEach((item) => {

  // Create card element with the name dataset
  const card = document.createElement('div');
  card.classList.add('card');
  card.dataset.name = item.name;


  // Create front of card
  let front = document.createElement('div');
  front.classList.add('front');


  // Create back of card, which contains
  let back = document.createElement('div');
  back.classList.add('back');
  back.style.backgroundImage = `url(${item.img})`;
  

  // Append card to grid, and front and back to each card
  grid.appendChild(card);
  card.appendChild(front);
  card.appendChild(back);
});





// Add event listener to grid
grid.addEventListener('click', function(event){
  // The event target is our clicked item 
  let clicked = event.target;

  // Do not allow the grid section itself to be selected; only select divs inside the grid
  if(
  clicked.nodeName === 'SECTION' || 
  clicked === previousTarget ||
  clicked.parentNode.classList.contains('selected') ||
  clicked.parentNode.classList.contains('match')
  ){

    return;
  }
 

  if(count<2){
    count++;

    if(count === 1){
      // Assign first guess
      firstGuess = clicked.parentNode.dataset.name
      console.log(firstGuess)
      clicked.parentNode.classList.add("selected");
    } else {
      // Assign second guess
      secondGuess = clicked.parentNode.dataset.name
      console.log(secondGuess)
      clicked.parentNode.classList.add('selected')
    }


    // If both guesses are not empty 
    if(firstGuess !== '' && secondGuess !== ''){
      // and the first guess matches the second match

      if(firstGuess === secondGuess){
        // run the match function atch function 
        console.log('appel');
      score.increment();

      setTimeout(score.match(), delay) 
      setTimeout(resetGuesses, delay)
     } else {
       setTimeout(resetGuesses, delay)
     }
    }

    // Set previous target to clicked
    previousTarget = clicked;
    
  }

})


// Add match CSS




let resetGuesses = () => {
 firstGuess = '';
 secondGuess = '';
 count = 0;
 previousTarget = null;


 let selected = document.querySelectorAll('.selected')
 selected.forEach((card) => {
   card.classList.remove('selected')
 });
};

// let instance_timer = new Timer(90)
// instance_timer.time();
// instance_timer.toHTML();

// // console.log(instance_timer.time())


// // timer

let second = 90; 

let timeLeft = document.querySelector('.timer')

// timeLeft.classList.add('timer')

function timer(){
  second--; 

  if(second <= 0 ) {
    alert('Game over');
  }
  // else if{}
  
  else{
    setTimeout(timer, 1000);
  }

  timeLeft.innerHTML = second + ' ' + "secondes"
  // console.log(second)
}


timer()


